const path = require('path');

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  outputDir: 'dist',
  productionSourceMap: false,
  configureWebpack: {
    devtool: 'source-map',
    resolve: {
      alias: {
        '@components': path.resolve(__dirname, 'src/components'),
        '@assets': path.resolve(__dirname, 'src/assets'),
      }
    },
    optimization: {
      minimize: false,
    }
  },
  css: {
    extract: true,
    sourceMap: true
  },
  devServer: {
    proxy: 'http://localhost:5000',
    port: 8080
  }
};
