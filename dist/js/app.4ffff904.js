/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"app": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"chunk-vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("56d7");


/***/ }),

/***/ "049d":
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":8,\"name\":\"Albania\"},{\"id\":19,\"name\":\"Austria\"},{\"id\":13,\"name\":\"Belgium\"},{\"id\":6,\"name\":\"Croatia\"},{\"id\":22,\"name\":\"Czech Republic\"},{\"id\":10,\"name\":\"Denmark\"},{\"id\":12,\"name\":\"England\"},{\"id\":9,\"name\":\"France\"},{\"id\":1,\"name\":\"Germany\"},{\"id\":3,\"name\":\"Hungary\"},{\"id\":24,\"name\":\"Iceland\"},{\"id\":7,\"name\":\"Italy\"},{\"id\":17,\"name\":\"Poland\"},{\"id\":23,\"name\":\"Portugal\"},{\"id\":18,\"name\":\"Republic of Ireland\"},{\"id\":15,\"name\":\"Romania\"},{\"id\":11,\"name\":\"Scotland\"},{\"id\":5,\"name\":\"Spain\"},{\"id\":14,\"name\":\"Slovakia\"},{\"id\":21,\"name\":\"Turkey\"},{\"id\":16,\"name\":\"Ukraine\"},{\"id\":2,\"name\":\"Switzerland\"},{\"id\":4,\"name\":\"Netherlands\"}]");

/***/ }),

/***/ "04a0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/leaderboard.91d10bfb.png";

/***/ }),

/***/ "112c":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Login_vue_vue_type_style_index_0_id_12e66a36_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3c7b");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Login_vue_vue_type_style_index_0_id_12e66a36_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Login_vue_vue_type_style_index_0_id_12e66a36_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "2000":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "3834":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_App_vue_vue_type_style_index_0_id_38259ad0_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("7ed9");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_App_vue_vue_type_style_index_0_id_38259ad0_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_App_vue_vue_type_style_index_0_id_38259ad0_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "3c00":
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":37,\"group\":\"Group 2\",\"teamA\":\"Switzerland\",\"flagA\":\"ch\",\"teamB\":\"Italy\",\"flagB\":\"it\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"29/6 23:00\",\"round\":4},{\"id\":38,\"group\":\"Group 2\",\"teamA\":\"Germany\",\"flagA\":\"de\",\"teamB\":\"Denmark\",\"flagB\":\"dk\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"30/6 02:00\",\"round\":4},{\"id\":39,\"group\":\"Group 2\",\"teamA\":\"England\",\"flagA\":\"gb-eng\",\"teamB\":\"Slovakia\",\"flagB\":\"sk\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"30/6 23:00\",\"round\":4},{\"id\":40,\"group\":\"Group 2\",\"teamA\":\"Spain\",\"flagA\":\"es\",\"teamB\":\"Georgia\",\"flagB\":\"ge\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"1/7 02:00\",\"round\":4},{\"id\":41,\"group\":\"Group 2\",\"teamA\":\"France\",\"flagA\":\"fr\",\"teamB\":\"Belgium\",\"flagB\":\"be\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"1/7 23:00\",\"round\":4},{\"id\":42,\"group\":\"Group 2\",\"teamA\":\"Portugal\",\"flagA\":\"pt\",\"teamB\":\"Slovenia\",\"flagB\":\"si\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"2/7 02:00\",\"round\":4},{\"id\":43,\"group\":\"Group 2\",\"teamA\":\"Romania\",\"flagA\":\"ro\",\"teamB\":\"Netherlands\",\"flagB\":\"nl\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"2/7 23:00\",\"round\":4},{\"id\":44,\"group\":\"Group 2\",\"teamA\":\"Austria\",\"flagA\":\"at\",\"teamB\":\"Turkey\",\"flagB\":\"tr\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"3/7 02:00\",\"round\":4}]");

/***/ }),

/***/ "3c7b":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "51d2":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Rules_vue_vue_type_style_index_0_id_6e4b4400_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("2000");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Rules_vue_vue_type_style_index_0_id_6e4b4400_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Rules_vue_vue_type_style_index_0_id_6e4b4400_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "53f1":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "56d7":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.runtime.esm-bundler.js + 3 modules
var vue_runtime_esm_bundler = __webpack_require__("7a23");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader-v16/dist/templateLoader.js??ref--6!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/App.vue?vue&type=template&id=38259ad0

const _hoisted_1 = ["src"];
const _hoisted_2 = ["src"];
const _hoisted_3 = ["src"];
const _hoisted_4 = {
  class: "container"
};
const _hoisted_5 = {
  class: "tab-buttons"
};
const _hoisted_6 = /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", null, "|", -1);
const _hoisted_7 = /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "40px",
    "font-weight": "bold"
  }
}, "EURO 2024"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "margin-top": "-10px",
    "font-weight": "bold"
  }
}, "TRUST YOUR GUTS, NOT THE FACTS")], -1);
const _hoisted_8 = {
  class: "tab-content"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("header", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
    class: "left-logo",
    src: __webpack_require__("8105")
  }, null, 8, _hoisted_1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
    class: "center-logo",
    src: __webpack_require__("9d49")
  }, null, 8, _hoisted_2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("a", {
    href: "/login",
    onClick: _cache[0] || (_cache[0] = Object(vue_runtime_esm_bundler["K" /* withModifiers */])((...args) => $options.goToLogin && $options.goToLogin(...args), ["prevent"]))
  }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
    class: "profile-circle",
    src: __webpack_require__("fe23"),
    alt: "Profile"
  }, null, 8, _hoisted_3)])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_4, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_5, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])({
      active: $data.currentTab === 'Leaderboard'
    }),
    onClick: _cache[1] || (_cache[1] = $event => $data.currentTab = 'Leaderboard')
  }, "LEADERBOARD", 2), _hoisted_6, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])({
      active: $data.currentTab === 'Rules'
    }),
    onClick: _cache[2] || (_cache[2] = $event => $data.currentTab = 'Rules')
  }, "RULES", 2)]), _hoisted_7]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_8, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["d" /* createBlock */])(Object(vue_runtime_esm_bundler["A" /* resolveDynamicComponent */])($options.currentTabComponent)))])])]);
}
// CONCATENATED MODULE: ./src/App.vue?vue&type=template&id=38259ad0

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.push.js
var es_array_push = __webpack_require__("14d9");

// EXTERNAL MODULE: ./src/assets/Txt-Rules.png
var Txt_Rules = __webpack_require__("f61c");
var Txt_Rules_default = /*#__PURE__*/__webpack_require__.n(Txt_Rules);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader-v16/dist/templateLoader.js??ref--6!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Rules.vue?vue&type=template&id=6e4b4400&scoped=true


const _withScopeId = n => (Object(vue_runtime_esm_bundler["w" /* pushScopeId */])("data-v-6e4b4400"), n = n(), Object(vue_runtime_esm_bundler["u" /* popScopeId */])(), n);
const Rulesvue_type_template_id_6e4b4400_scoped_true_hoisted_1 = {
  key: 0,
  class: "imgPad"
};
const Rulesvue_type_template_id_6e4b4400_scoped_true_hoisted_2 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
  class: "registration-Rules",
  src: Txt_Rules_default.a,
  alt: "Registration Rules"
}, null, -1));
const Rulesvue_type_template_id_6e4b4400_scoped_true_hoisted_3 = [Rulesvue_type_template_id_6e4b4400_scoped_true_hoisted_2];
function Rulesvue_type_template_id_6e4b4400_scoped_true_render(_ctx, _cache, $props, $setup, $data, $options) {
  return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", null, [$data.currentTab === 'Rules' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Rulesvue_type_template_id_6e4b4400_scoped_true_hoisted_1, Rulesvue_type_template_id_6e4b4400_scoped_true_hoisted_3)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)]);
}
// CONCATENATED MODULE: ./src/components/Rules.vue?vue&type=template&id=6e4b4400&scoped=true

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Rules.vue?vue&type=script&lang=js

/* harmony default export */ var Rulesvue_type_script_lang_js = ({
  data() {
    return {
      currentTab: 'Rules'
    };
  },
  async created() {},
  computed: {},
  methods: {}
});
// CONCATENATED MODULE: ./src/components/Rules.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./src/components/Rules.vue?vue&type=style&index=0&id=6e4b4400&scoped=true&lang=css
var Rulesvue_type_style_index_0_id_6e4b4400_scoped_true_lang_css = __webpack_require__("51d2");

// EXTERNAL MODULE: ./node_modules/vue-loader-v16/dist/exportHelper.js
var exportHelper = __webpack_require__("6b0d");
var exportHelper_default = /*#__PURE__*/__webpack_require__.n(exportHelper);

// CONCATENATED MODULE: ./src/components/Rules.vue







const __exports__ = /*#__PURE__*/exportHelper_default()(Rulesvue_type_script_lang_js, [['render',Rulesvue_type_template_id_6e4b4400_scoped_true_render],['__scopeId',"data-v-6e4b4400"]])

/* harmony default export */ var Rules = (__exports__);
// EXTERNAL MODULE: ./src/assets/leaderboard.png
var leaderboard = __webpack_require__("04a0");
var leaderboard_default = /*#__PURE__*/__webpack_require__.n(leaderboard);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader-v16/dist/templateLoader.js??ref--6!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Leaderboard.vue?vue&type=template&id=2ae074ba&scoped=true


const Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId = n => (Object(vue_runtime_esm_bundler["w" /* pushScopeId */])("data-v-2ae074ba"), n = n(), Object(vue_runtime_esm_bundler["u" /* popScopeId */])(), n);
const Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_1 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "full-width-image-wrapper",
  style: {
    "margin-top": "30px"
  }
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
  class: "full-width-image",
  src: leaderboard_default.a,
  alt: "Leaderboard"
})], -1));
const Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_2 = {
  class: "container"
};
const Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_3 = {
  key: 0,
  class: "loading-modal"
};
const Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_4 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-content"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-spinner"
}, "loading...")], -1));
const Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_5 = [Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_4];
const Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_6 = {
  key: 1,
  class: "table-container"
};
const Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_7 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("thead", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tr", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", {
  style: {
    "font-size": "10px"
  }
}, "RANK"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", {
  style: {
    "font-size": "10px"
  }
}, "NAME"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", null, "PTS")])], -1));
const Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_8 = ["onClick"];
const _hoisted_9 = {
  key: 0,
  class: "modal"
};
const _hoisted_10 = {
  class: "modal-content"
};
const _hoisted_11 = {
  class: "modal-header"
};
const _hoisted_12 = {
  class: "user-info"
};
const _hoisted_13 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("br", null, null, -1));
const _hoisted_14 = {
  class: "score"
};
const _hoisted_15 = {
  class: "tabs"
};
const _hoisted_16 = {
  class: "tab-header-wrapper"
};
const _hoisted_17 = {
  class: "tab-header"
};
const _hoisted_18 = {
  class: "tab-content"
};
const _hoisted_19 = {
  class: "table-header"
};
const _hoisted_20 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION", -1));
const _hoisted_21 = {
  style: {
    "font-size": "10px",
    "font-weight": "bold"
  }
};
const _hoisted_22 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const _hoisted_23 = {
  class: "table2"
};
const _hoisted_24 = {
  key: 0,
  class: "match-row"
};
const _hoisted_25 = {
  class: "td_1"
};
const _hoisted_26 = {
  class: "td_2"
};
const _hoisted_27 = {
  class: "input-container"
};
const _hoisted_28 = ["onUpdate:modelValue"];
const _hoisted_29 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const _hoisted_30 = {
  class: "td_4"
};
const _hoisted_31 = {
  class: "input-container"
};
const _hoisted_32 = ["onUpdate:modelValue"];
const _hoisted_33 = {
  class: "td_5"
};
const _hoisted_34 = {
  class: "tab-content"
};
const _hoisted_35 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "table-header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION")], -1));
const _hoisted_36 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const _hoisted_37 = {
  class: "table2"
};
const _hoisted_38 = {
  key: 0,
  class: "match-row"
};
const _hoisted_39 = {
  class: "td_1"
};
const _hoisted_40 = {
  class: "td_2"
};
const _hoisted_41 = {
  class: "input-container"
};
const _hoisted_42 = ["onUpdate:modelValue"];
const _hoisted_43 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const _hoisted_44 = {
  class: "td_4"
};
const _hoisted_45 = {
  class: "input-container"
};
const _hoisted_46 = ["onUpdate:modelValue"];
const _hoisted_47 = {
  class: "td_5"
};
const _hoisted_48 = {
  class: "tab-content"
};
const _hoisted_49 = /*#__PURE__*/Leaderboardvue_type_template_id_2ae074ba_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", null, "Content for Tab 3", -1));
const _hoisted_50 = [_hoisted_49];
function Leaderboardvue_type_template_id_2ae074ba_scoped_true_render(_ctx, _cache, $props, $setup, $data, $options) {
  return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", null, [Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_1, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_2, [$data.loading ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_3, Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_5)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), !$data.loading && $data.allUsersData ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_6, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_7, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($options.rankedUsers, (user, index) => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", {
      key: user.id
    }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(user.rank), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
      onClick: $event => $options.showMatchData(user.match_data)
    }, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(user.first_name), 9, Leaderboardvue_type_template_id_2ae074ba_scoped_true_hoisted_8), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(user.TotalScore), 1)]);
  }), 128))])])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)]), $data.showModal ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", _hoisted_9, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_10, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_11, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_12, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", null, [Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.selectedUser.first_name), 1), _hoisted_13, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("b", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.selectedUser.selected_team_name), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", _hoisted_14, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.selectedUser.TotalScore) + " PTS", 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
    class: "close",
    onClick: _cache[0] || (_cache[0] = (...args) => $options.closeModal && $options.closeModal(...args))
  }, "×")]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_15, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_16, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_17, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.selectedTab === 1
    }]),
    onClick: _cache[1] || (_cache[1] = $event => $data.selectedTab = 1)
  }, "QUALIFICATION", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.selectedTab === 2
    }]),
    onClick: _cache[2] || (_cache[2] = $event => $data.selectedTab = 2)
  }, "ROUND OF 16 FIXTURES", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.selectedTab === 3
    }]),
    onClick: _cache[3] || (_cache[3] = $event => $data.selectedTab = 3),
    disabled: ""
  }, "QUARTER-FINALS", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.selectedTab === 4
    }]),
    onClick: _cache[4] || (_cache[4] = $event => $data.selectedTab = 4),
    disabled: ""
  }, "SEMI-FINALS", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.selectedTab === 5
    }]),
    onClick: _cache[5] || (_cache[5] = $event => $data.selectedTab = 5),
    disabled: ""
  }, "FINAL", 2)])]), Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_18, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])([1, 2, 3], round => {
    return Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
      class: "table-container",
      key: round
    }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_19, [_hoisted_20, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", _hoisted_21, "MATCH " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(round) + " OF 3", 1)]), _hoisted_22, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", _hoisted_23, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.selectedMatchData, match => {
      return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
        key: match.id
      }, [match.round === round ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", _hoisted_24, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_25, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_26, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_27, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        disabled: "",
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionA = $event
      }, null, 8, _hoisted_28), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])])]), _hoisted_29, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_30, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_31, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        disabled: "",
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionB = $event
      }, null, 8, _hoisted_32), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_33, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
    }), 128))])])]);
  }), 64))], 512), [[vue_runtime_esm_bundler["H" /* vShow */], $data.selectedTab === 1]]), Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_34, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])([4], round => {
    return Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
      class: "table-container",
      key: round
    }, [_hoisted_35, _hoisted_36, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", _hoisted_37, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.selectedMatchData, match => {
      return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
        key: match.id
      }, [match.round === round ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", _hoisted_38, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_39, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_40, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_41, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        disabled: "",
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionA = $event
      }, null, 8, _hoisted_42), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])])]), _hoisted_43, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_44, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_45, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        disabled: "",
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionB = $event
      }, null, 8, _hoisted_46), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_47, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
    }), 128))])])]);
  }), 64))], 512), [[vue_runtime_esm_bundler["H" /* vShow */], $data.selectedTab === 2]]), Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_48, _hoisted_50, 512), [[vue_runtime_esm_bundler["H" /* vShow */], $data.selectedTab === 3]])])])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)]);
}
// CONCATENATED MODULE: ./src/components/Leaderboard.vue?vue&type=template&id=2ae074ba&scoped=true

// EXTERNAL MODULE: ./node_modules/axios/lib/axios.js + 43 modules
var axios = __webpack_require__("cee4");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Leaderboard.vue?vue&type=script&lang=js

/* harmony default export */ var Leaderboardvue_type_script_lang_js = ({
  data() {
    return {
      currentTab: 'Rules',
      resultMatches: [],
      allUsersData: [],
      selectedUser: null,
      showModal: false,
      selectedTab: 1,
      selectedMatchData: [],
      loading: false,
      TeamChame: {},
      useTest: false,
      // Flag to determine if test data should be used
      // baseUrl: 'https://euro2024.egency.fun/test',
      baseUrl: 'https://euro2024.egency.fun'
    };
  },
  async created() {
    await this.fetchResultMatches();
    await this.fetchResultCham();
    await this.fetchData();
    this.calculateScores();
  },
  computed: {
    rankedUsers() {
      // First, sort users by TotalScore (descending) and then by first_name (ascending)
      const sortedUsers = this.allUsersData.slice().sort((a, b) => {
        if (b.TotalScore === a.TotalScore) {
          return a.first_name.localeCompare(b.first_name);
        }
        return b.TotalScore - a.TotalScore;
      });

      // Assign ranks
      let rank = 1;
      let previousScore = null;
      let rankAdjustment = 0;
      return sortedUsers.map((user, index, array) => {
        if (previousScore !== null && user.TotalScore < previousScore) {
          rank += 1 + rankAdjustment;
          rankAdjustment = 0;
        } else if (previousScore !== null && user.TotalScore === previousScore) {
          rankAdjustment += 1;
        } else {
          rankAdjustment = 0;
        }
        previousScore = user.TotalScore;
        return {
          ...user,
          rank
        };
      });
    }
  },
  methods: {
    async fetchResultMatches() {
      try {
        const response = await axios["a" /* default */].get(this.baseUrl + '/json/resultMatchs.json');
        this.resultMatches = response.data;
      } catch (error) {
        console.error('Error fetching result matches data:', error);
      }
    },
    async fetchResultCham() {
      try {
        const response = await axios["a" /* default */].get(this.baseUrl + '/json/resultCham.json');
        this.TeamChame = response.data;
      } catch (error) {
        console.error('Error fetching result cham data:', error);
      }
    },
    async fetchData() {
      this.loading = true;
      try {
        const response = this.useTest ? {
          data: testData
        } : await axios["a" /* default */].get(this.baseUrl + '/api/get_users.php'); // Use test data if flag is set
        this.allUsersData = response.data.data;
      } catch (error) {
        console.error('Error fetching user data:', error);
        alert('Error fetching data');
      } finally {
        this.loading = false;
      }
    },
    calculateScores() {
      for (let user of this.allUsersData) {
        let score = 0;
        const data = JSON.parse(user.match_data);
        for (let Match of this.resultMatches) {
          const predictionMatch = data.find(m => m.id == Match.id && m.predictionA !== '' && m.predictionB !== '');
          if (!predictionMatch) continue;
          if (Match.predictionA === '' || Match.predictionB === '' || predictionMatch.predictionA === '' || predictionMatch.predictionB === '') continue;

          // win 3 score
          if (predictionMatch.predictionA == Match.predictionA && predictionMatch.predictionB == Match.predictionB) {
            predictionMatch.group !== 'Group 2' ? score += 3 : score += 6;
          }
          let winOrLostOrDrawPrediction = 3;
          let winOrLostOrDrawResult = 3;
          winOrLostOrDrawPrediction = predictionMatch.predictionA == predictionMatch.predictionB ? 2 : predictionMatch.predictionA > predictionMatch.predictionB ? 1 : predictionMatch.predictionA < predictionMatch.predictionB ? 0 : 3;
          winOrLostOrDrawResult = Match.predictionA == Match.predictionB ? 2 : Match.predictionA > Match.predictionB ? 1 : Match.predictionA < Match.predictionB ? 0 : 3;
          // win 1 score
          if (user.first_name == 'A+' && predictionMatch.group == 'Group 2') {
            debugger;
          }
          console.log(user.first_name);
          if (winOrLostOrDrawPrediction == winOrLostOrDrawResult && winOrLostOrDrawPrediction != 3 && winOrLostOrDrawResult != 3) {
            predictionMatch.group !== 'Group 2' ? score += 1 : score += 2;
          }
        }
        // win 12 score
        if (user.selected_team_name == this.TeamChame.name) {
          score += 12;
        }
        user.TotalScore = score;
      }
      this.allUsersData.sort((a, b) => b.TotalScore - a.TotalScore);
    },
    useTestData() {
      this.useTest = true;
      this.fetchData().then(() => this.calculateScores());
    },
    showMatchData(matchData) {
      this.selectedMatchData = JSON.parse(matchData);
      this.selectedUser = this.allUsersData.find(user => user.match_data === matchData);
      this.showModal = true;
    },
    closeModal() {
      this.showModal = false;
      this.selectedMatchData = [];
    }
  }
});
// CONCATENATED MODULE: ./src/components/Leaderboard.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./src/components/Leaderboard.vue?vue&type=style&index=0&id=2ae074ba&scoped=true&lang=css
var Leaderboardvue_type_style_index_0_id_2ae074ba_scoped_true_lang_css = __webpack_require__("ea0f");

// CONCATENATED MODULE: ./src/components/Leaderboard.vue







const Leaderboard_exports_ = /*#__PURE__*/exportHelper_default()(Leaderboardvue_type_script_lang_js, [['render',Leaderboardvue_type_template_id_2ae074ba_scoped_true_render],['__scopeId',"data-v-2ae074ba"]])

/* harmony default export */ var Leaderboard = (Leaderboard_exports_);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader-v16/dist/templateLoader.js??ref--6!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Login.vue?vue&type=template&id=12e66a36&scoped=true

const Loginvue_type_template_id_12e66a36_scoped_true_withScopeId = n => (Object(vue_runtime_esm_bundler["w" /* pushScopeId */])("data-v-12e66a36"), n = n(), Object(vue_runtime_esm_bundler["u" /* popScopeId */])(), n);
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_1 = {
  class: "container"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_2 = {
  key: 0
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_3 = {
  class: "form-container"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_4 = {
  class: "form-group"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_5 = {
  class: "form-group"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_6 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
  type: "submit",
  class: "login-button"
}, "LOGIN", -1));
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_7 = {
  key: 0,
  class: "loading-modal"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_8 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-content"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-spinner"
}, "loading...")], -1));
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_9 = [Loginvue_type_template_id_12e66a36_scoped_true_hoisted_8];
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_10 = {
  key: 1
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_11 = {
  class: "tabs-container"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_12 = {
  class: "tabs"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_13 = {
  key: 0
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_14 = {
  class: "table-header"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_15 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  class: "table-title"
}, "SCORE PREDICTION", -1));
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_16 = {
  class: "table-subtitle"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_17 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  class: "table-divider"
}, null, -1));
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_18 = {
  key: 0,
  class: "match-row"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_19 = {
  class: "td_1"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_20 = {
  class: "td_2"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_21 = ["onUpdate:modelValue"];
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_22 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_23 = {
  class: "td_4"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_24 = ["onUpdate:modelValue"];
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_25 = {
  class: "td_5"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_26 = {
  key: 1
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_27 = {
  class: "table-container"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_28 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "table-header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  class: "table-title"
}, "SCORE PREDICTION")], -1));
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_29 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  class: "table-divider"
}, null, -1));
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_30 = {
  key: 0,
  class: "match-row"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_31 = {
  class: "td_1"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_32 = {
  class: "td_2"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_33 = ["onUpdate:modelValue"];
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_34 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_35 = {
  class: "td_4"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_36 = ["onUpdate:modelValue"];
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_37 = {
  class: "td_5"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_38 = {
  key: 0,
  class: "match-row"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_39 = {
  class: "td_1"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_40 = {
  class: "td_2"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_41 = ["onUpdate:modelValue"];
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_42 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_43 = {
  class: "td_4"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_44 = ["onUpdate:modelValue"];
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_45 = {
  class: "td_5"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_46 = {
  key: 1,
  class: "match-row"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_47 = {
  class: "td_1"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_48 = {
  class: "td_2"
};
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_49 = ["onUpdate:modelValue"];
const Loginvue_type_template_id_12e66a36_scoped_true_hoisted_50 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const _hoisted_51 = {
  class: "td_4"
};
const _hoisted_52 = ["onUpdate:modelValue"];
const _hoisted_53 = {
  class: "td_5"
};
const _hoisted_54 = {
  key: 2,
  class: "match-row"
};
const _hoisted_55 = {
  class: "td_1"
};
const _hoisted_56 = {
  class: "td_2"
};
const _hoisted_57 = ["onUpdate:modelValue"];
const _hoisted_58 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const _hoisted_59 = {
  class: "td_4"
};
const _hoisted_60 = ["onUpdate:modelValue"];
const _hoisted_61 = {
  class: "td_5"
};
const _hoisted_62 = {
  key: 3,
  class: "match-row"
};
const _hoisted_63 = {
  class: "td_1"
};
const _hoisted_64 = {
  class: "td_2"
};
const _hoisted_65 = ["onUpdate:modelValue"];
const _hoisted_66 = /*#__PURE__*/Loginvue_type_template_id_12e66a36_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const _hoisted_67 = {
  class: "td_4"
};
const _hoisted_68 = ["onUpdate:modelValue"];
const _hoisted_69 = {
  class: "td_5"
};
const _hoisted_70 = {
  class: "submit-container"
};
const _hoisted_71 = ["disabled"];
function Loginvue_type_template_id_12e66a36_scoped_true_render(_ctx, _cache, $props, $setup, $data, $options) {
  return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_1, [!$data.userData ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_2, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_3, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("form", {
    onSubmit: _cache[2] || (_cache[2] = Object(vue_runtime_esm_bundler["K" /* withModifiers */])((...args) => $options.login && $options.login(...args), ["prevent"]))
  }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_4, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
    placeholder: "Username",
    type: "text",
    id: "username",
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $data.username = $event),
    required: ""
  }, null, 512), [[vue_runtime_esm_bundler["G" /* vModelText */], $data.username]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_5, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
    placeholder: "Password",
    type: "password",
    id: "password",
    "onUpdate:modelValue": _cache[1] || (_cache[1] = $event => $data.password = $event),
    required: ""
  }, null, 512), [[vue_runtime_esm_bundler["G" /* vModelText */], $data.password]])]), Loginvue_type_template_id_12e66a36_scoped_true_hoisted_6], 32)]), $data.loading ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_7, Loginvue_type_template_id_12e66a36_scoped_true_hoisted_9)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), $data.userData ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_10, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_11, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_12, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.activeTab === 'group1'
    }]),
    onClick: _cache[3] || (_cache[3] = $event => $options.changeTab('group1'))
  }, "QUALIFICATION", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.activeTab === 'group2'
    }]),
    onClick: _cache[4] || (_cache[4] = $event => $options.changeTab('group2'))
  }, "ROUND OF 16 FIXTURES", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.activeTab === 'group3'
    }]),
    onClick: _cache[5] || (_cache[5] = $event => $options.changeTab('group3')),
    disabled: ""
  }, "QUARTER-FINALS", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.activeTab === 'group4'
    }]),
    onClick: _cache[6] || (_cache[6] = $event => $options.changeTab('group4')),
    disabled: ""
  }, "SEMI-FINALS", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.activeTab === 'group5'
    }]),
    onClick: _cache[7] || (_cache[7] = $event => $options.changeTab('group5')),
    disabled: ""
  }, "FINAL", 2)])]), $data.activeTab === 'group1' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_13, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.totalRounds, round => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", {
      class: "table-container",
      key: round
    }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_14, [Loginvue_type_template_id_12e66a36_scoped_true_hoisted_15, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_16, "MATCH " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(round) + " OF " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.totalRounds), 1)]), Loginvue_type_template_id_12e66a36_scoped_true_hoisted_17, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.parsedMatchData, match => {
      return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
        key: match.id
      }, [match.round === round ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_18, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_19, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_20, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionA = $event,
        disabled: ""
      }, null, 8, Loginvue_type_template_id_12e66a36_scoped_true_hoisted_21), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])]), Loginvue_type_template_id_12e66a36_scoped_true_hoisted_22, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_23, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionB = $event,
        disabled: ""
      }, null, 8, Loginvue_type_template_id_12e66a36_scoped_true_hoisted_24), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_25, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
    }), 128))])])]);
  }), 128))])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), $data.activeTab === 'group2' || $data.activeTab === 'group3' || $data.activeTab === 'group4' || $data.activeTab === 'group5' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_26, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("form", {
    onSubmit: _cache[8] || (_cache[8] = Object(vue_runtime_esm_bundler["K" /* withModifiers */])((...args) => $options.submitForm && $options.submitForm(...args), ["prevent"]))
  }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_27, [Loginvue_type_template_id_12e66a36_scoped_true_hoisted_28, Loginvue_type_template_id_12e66a36_scoped_true_hoisted_29, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [!$options.hasGroup2($data.parsedMatchData) ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
    key: 0
  }, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.matches, match => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
      key: match.id
    }, [$options.isFutureMatch(match.time) ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_30, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_31, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_32, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event
    }, null, 8, Loginvue_type_template_id_12e66a36_scoped_true_hoisted_33), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])]), Loginvue_type_template_id_12e66a36_scoped_true_hoisted_34, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_35, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event
    }, null, 8, Loginvue_type_template_id_12e66a36_scoped_true_hoisted_36), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_37, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
  }), 128)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), $options.hasGroup2($data.parsedMatchData) ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
    key: 1
  }, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.parsedMatchData, match => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
      key: match.id
    }, [match.group == 'Group 2' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_38, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_39, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_40, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event,
      disabled: ""
    }, null, 8, Loginvue_type_template_id_12e66a36_scoped_true_hoisted_41), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])]), Loginvue_type_template_id_12e66a36_scoped_true_hoisted_42, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_43, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event,
      disabled: ""
    }, null, 8, Loginvue_type_template_id_12e66a36_scoped_true_hoisted_44), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_45, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), match.group == 'Group 3' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_46, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_47, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Loginvue_type_template_id_12e66a36_scoped_true_hoisted_48, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event,
      disabled: ""
    }, null, 8, Loginvue_type_template_id_12e66a36_scoped_true_hoisted_49), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])]), Loginvue_type_template_id_12e66a36_scoped_true_hoisted_50, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_51, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event,
      disabled: ""
    }, null, 8, _hoisted_52), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_53, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), match.group == 'Group 4' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", _hoisted_54, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_55, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_56, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event,
      disabled: ""
    }, null, 8, _hoisted_57), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])]), _hoisted_58, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_59, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event,
      disabled: ""
    }, null, 8, _hoisted_60), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_61, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), match.group == 'Group 5' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", _hoisted_62, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_63, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_64, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event,
      disabled: ""
    }, null, 8, _hoisted_65), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])]), _hoisted_66, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_67, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event,
      disabled: ""
    }, null, 8, _hoisted_68), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", _hoisted_69, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
  }), 128)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", _hoisted_70, [!$options.hasGroup2($data.parsedMatchData) ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("button", {
    key: 0,
    type: "submit",
    class: "submit-button",
    disabled: $data.formSubmitted
  }, "SUBMIT", 8, _hoisted_71)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)])], 32)])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)]);
}
// CONCATENATED MODULE: ./src/components/Login.vue?vue&type=template&id=12e66a36&scoped=true

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Login.vue?vue&type=script&lang=js


const matchesDataGroup2 = __webpack_require__("3c00");
const matchesDataGroup3 = __webpack_require__("73dc");
const matchesDataGroup4 = __webpack_require__("e52a");
const matchesDataGroup5 = __webpack_require__("6f11");
/* harmony default export */ var Loginvue_type_script_lang_js = ({
  name: 'Login',
  data() {
    return {
      username: '',
      password: '',
      userData: null,
      parsedMatchData: [],
      loading: false,
      totalRounds: 3,
      // Adjust this according to the total number of rounds
      activeTab: 'group1',
      matches: [],
      // Default to matchesDataGroup2
      formSubmitted: false,
      currentTab: 'Rules',
      // Default to Rules tab
      baseUrl: 'https://euro2024.egency.fun',
      // baseUrl: 'https://euro2024.egency.fun/test',
      flageLogin: false
    };
  },
  async created() {
    if (localStorage.hasOwnProperty('userData')) {
      await this.login();
    }
  },
  methods: {
    async login() {
      this.loading = true;
      try {
        if (localStorage.hasOwnProperty('userData')) {
          const storedUserData = localStorage.getItem("userData");
          this.userData = JSON.parse(storedUserData);
          this.parsedMatchData = JSON.parse(this.userData.match_data);
          console.log('this.userData', this.userData);
          console.log('this.parsedMatchData', this.parsedMatchData);
          this.loading = false;
          return;
        }
        const response = await axios["a" /* default */].post(this.baseUrl + '/api/login.php', {
          username: this.username,
          password: this.password
        }, {
          headers: {
            'Content-Type': 'application/json'
          }
        });
        if (response.data.error) {
          alert(response.data.message);
        } else {
          this.flageLogin = true;
          console.log('Login Success:', response.data);
          this.userData = response.data.data;
          this.parsedMatchData = JSON.parse(this.userData.match_data);
          localStorage.setItem("userData", JSON.stringify(this.userData));
        }
      } catch (error) {
        console.error('Login Error:', error);
        alert('Error logging in. Please try again.');
      } finally {
        this.loading = false;
      }
    },
    changeTab(tab) {
      this.activeTab = tab;
      if (tab === 'group2') {
        this.matches = matchesDataGroup2;
      } else if (tab === 'group3') {
        this.matches = matchesDataGroup3;
      } else if (tab === 'group4') {
        this.matches = matchesDataGroup4;
      } else if (tab === 'group5') {
        this.matches = matchesDataGroup5;
      }
      this.formSubmitted = this.parsedMatchData.some(match => match.group === this.activeTab.replace('group', 'Group'));
    },
    isTodayInRange(startDate, endDate) {
      const today = new Date();
      const start = new Date(startDate);
      const end = new Date(endDate);
      return today >= start && today <= end;
    },
    hasGroup2(data) {
      return data.some(match => match.group === 'Group 2');
    },
    isFutureMatch(matchTime) {
      const [dayMonth, time] = matchTime.split(' ');
      const [day, month] = dayMonth.split('/');
      const [hours, minutes] = time.split(':');
      const matchDate = new Date();
      matchDate.setDate(day);
      matchDate.setMonth(month - 1);
      matchDate.setHours(hours);
      matchDate.setMinutes(minutes);
      matchDate.setSeconds(0);
      console.log('matchDate', matchDate);
      console.log('new Date()', new Date());
      return matchDate > new Date();
    },
    async submitForm() {
      if (window.confirm("Confirm ?")) {
        this.loading = true;
        this.matches = this.matches.map(match => {
          if (match.time) {
            match.time = match.time.replace(/\\\//g, '/');
          }
          return match;
        });
        const formData = {
          first_name: this.userData.first_name,
          last_name: this.userData.last_name || ' ',
          matches: JSON.stringify(this.matches),
          file_data: this.userData.file_path || null,
          selected_team_id: this.userData.selected_team_id || null,
          selected_team_name: this.userData.selected_team_name || null
        };
        console.log('formData:', formData);
        try {
          const response = await axios["a" /* default */].post(this.baseUrl + '/api/save_matches.php', formData, {
            headers: {
              'Content-Type': 'application/json'
            }
          });
          console.log('response:', response);
          if (response.data.status === 'success') {
            alert('ขอบคุณที่ร่วมสนุก ขอให้โชคดี รวยทรัพย์รับโชค เงินทองไหลมาเทมา');
            this.formSubmitted = true;
            let match = JSON.parse(this.userData.match_data);
            match.push(...this.matches);
            this.userData.match_data = JSON.stringify(match);
            localStorage.setItem("userData", JSON.stringify(this.userData));
            const storedUserData = localStorage.getItem("userData");
            this.userData = JSON.parse(storedUserData);
            this.parsedMatchData = JSON.parse(this.userData.match_data);
          } else {
            alert(`บันทึกไม่สำเร็จ: ${response.data.message}`);
            console.error('Error:', response.data.message);
          }
        } catch (error) {
          if (error.response) {
            console.error('Error response:', error.response.data);
            console.error('Error status:', error.response.status);
            console.error('Error headers:', error.response.headers);
            alert(`บันทึกไม่สำเร็จ: ${response.data.message}`);
          } else if (error.request) {
            console.error('Error request:', error.request);
            alert('บันทึกไม่สำเร็จ: Network error. No response received.');
          } else {
            console.error('Error message:', error.message);
            alert(`บันทึกไม่สำเร็จ: ${error.message}`);
          }
        } finally {
          this.loading = false;
        }
      }
    }
  }
});
// CONCATENATED MODULE: ./src/components/Login.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./src/components/Login.vue?vue&type=style&index=0&id=12e66a36&scoped=true&lang=css
var Loginvue_type_style_index_0_id_12e66a36_scoped_true_lang_css = __webpack_require__("112c");

// CONCATENATED MODULE: ./src/components/Login.vue







const Login_exports_ = /*#__PURE__*/exportHelper_default()(Loginvue_type_script_lang_js, [['render',Loginvue_type_template_id_12e66a36_scoped_true_render],['__scopeId',"data-v-12e66a36"]])

/* harmony default export */ var Login = (Login_exports_);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/App.vue?vue&type=script&lang=js




/* harmony default export */ var Appvue_type_script_lang_js = ({
  data() {
    return {
      currentTab: 'Leaderboard'
    };
  },
  computed: {
    currentTabComponent() {
      if (this.currentTab === 'Rules') {
        return 'Rules';
      } else if (this.currentTab === 'Leaderboard') {
        return 'Leaderboard';
      } else if (this.currentTab === 'Login') {
        return 'Login';
      }
    }
  },
  components: {
    Rules: Rules,
    Leaderboard: Leaderboard,
    Login: Login
  },
  methods: {
    goToLogin(event) {
      event.preventDefault();
      this.currentTab = 'Login';
      this.$router.push('/login');
    }
  }
});
// CONCATENATED MODULE: ./src/App.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./src/App.vue?vue&type=style&index=0&id=38259ad0&lang=css
var Appvue_type_style_index_0_id_38259ad0_lang_css = __webpack_require__("3834");

// CONCATENATED MODULE: ./src/App.vue







const App_exports_ = /*#__PURE__*/exportHelper_default()(Appvue_type_script_lang_js, [['render',render]])

/* harmony default export */ var App = (App_exports_);
// EXTERNAL MODULE: ./node_modules/vue-router/dist/vue-router.mjs
var vue_router = __webpack_require__("6605");

// EXTERNAL MODULE: ./src/assets/Txt-Register.png
var Txt_Register = __webpack_require__("66e1");
var Txt_Register_default = /*#__PURE__*/__webpack_require__.n(Txt_Register);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader-v16/dist/templateLoader.js??ref--6!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Ero.vue?vue&type=template&id=1d411fb8&scoped=true



const Erovue_type_template_id_1d411fb8_scoped_true_withScopeId = n => (Object(vue_runtime_esm_bundler["w" /* pushScopeId */])("data-v-1d411fb8"), n = n(), Object(vue_runtime_esm_bundler["u" /* popScopeId */])(), n);
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_1 = /*#__PURE__*/Object(vue_runtime_esm_bundler["h" /* createStaticVNode */])("<div class=\"container\" data-v-1d411fb8><div class=\"header\" data-v-1d411fb8><p style=\"font-size:40px;font-weight:bold;\" data-v-1d411fb8>EURO 2024</p><p style=\"margin-top:-10px;font-weight:bold;\" data-v-1d411fb8>TRUST YOUR GUTS, NOT THE FACTS</p></div><img class=\"registration-Rules\" src=\"" + Txt_Rules_default.a + "\" alt=\"Registration\" data-v-1d411fb8></div><div class=\"registration-image-wrapper\" data-v-1d411fb8><img class=\"registration-image\" src=\"" + Txt_Register_default.a + "\" alt=\"Registration\" data-v-1d411fb8></div>", 2);
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_3 = {
  class: "container"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_4 = {
  class: "form-group"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_5 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("label", {
  for: "firstName"
}, "Name", -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_6 = {
  class: "form-group"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_7 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("label", {
  for: "file"
}, "Upload Slip", -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_8 = {
  class: "form-group"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_9 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("label", {
  for: "champion"
}, "Select Champion Team", -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_10 = ["value"];
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_11 = {
  class: "table-container"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_12 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "table-header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "10px",
    "font-weight": "bold"
  }
}, "MATCH 1 OF 3")], -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_13 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_14 = {
  key: 0,
  class: "match-row"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_15 = {
  class: "td_1"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_16 = {
  class: "td_2"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_17 = ["onUpdate:modelValue"];
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_18 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_19 = {
  class: "td_4"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_20 = ["onUpdate:modelValue"];
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_21 = {
  class: "td_5"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_22 = {
  class: "table-container"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_23 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "table-header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "10px",
    "font-weight": "bold"
  }
}, "MATCH 2 OF 3")], -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_24 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_25 = {
  key: 0,
  class: "match-row"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_26 = {
  class: "td_1"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_27 = {
  class: "td_2"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_28 = ["onUpdate:modelValue"];
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_29 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_30 = {
  class: "td_4"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_31 = ["onUpdate:modelValue"];
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_32 = {
  class: "td_5"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_33 = {
  class: "table-container"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_34 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "table-header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "10px",
    "font-weight": "bold"
  }
}, "MATCH 3 OF 3")], -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_35 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_36 = {
  key: 0,
  class: "match-row"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_37 = {
  class: "td_1"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_38 = {
  class: "td_2"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_39 = ["onUpdate:modelValue"];
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_40 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_41 = {
  class: "td_4"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_42 = ["onUpdate:modelValue"];
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_43 = {
  class: "td_5"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_44 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  style: {
    "padding": "15px"
  }
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
  type: "submit"
}, "SUBMIT")], -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_45 = {
  key: 1
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_46 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("h2", null, "SUMMARY OF MATCH RESULTS", -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_47 = {
  class: "table-container"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_48 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("thead", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tr", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", null, "id"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", null, "FirstName"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", null, "LastName"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", null, "Match Data"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", null, "Img")])], -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_49 = ["onClick"];
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_50 = ["src"];
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_51 = {
  key: 2,
  class: "modal"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_52 = {
  class: "modal-content"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_53 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("h2", null, "information", -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_54 = {
  class: "table-container"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_55 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("thead", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tr", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", {
  class: "team-column"
}, "Team A"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", {
  class: "team-column"
}, "Team B"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", null, "Time")])], -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_56 = {
  class: "team-column"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_57 = {
  class: "small-input"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_58 = {
  class: "small-input"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_59 = {
  class: "team-column"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_60 = {
  key: 3,
  class: "loading-modal"
};
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_61 = /*#__PURE__*/Erovue_type_template_id_1d411fb8_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-content"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-spinner"
}, "loading...")], -1));
const Erovue_type_template_id_1d411fb8_scoped_true_hoisted_62 = [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_61];
function Erovue_type_template_id_1d411fb8_scoped_true_render(_ctx, _cache, $props, $setup, $data, $options) {
  return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", null, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_1, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_3, [!$data.formSubmitted ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("form", {
    key: 0,
    onSubmit: _cache[3] || (_cache[3] = Object(vue_runtime_esm_bundler["K" /* withModifiers */])((...args) => $options.submitForm && $options.submitForm(...args), ["prevent"]))
  }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_4, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_5, Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
    type: "text",
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $data.firstName = $event),
    id: "firstName",
    placeholder: "Name"
  }, null, 512), [[vue_runtime_esm_bundler["G" /* vModelText */], $data.firstName]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_6, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_7, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
    type: "file",
    onChange: _cache[1] || (_cache[1] = (...args) => $options.onFileChange && $options.onFileChange(...args)),
    id: "file"
  }, null, 32)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_8, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_9, Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("select", {
    "onUpdate:modelValue": _cache[2] || (_cache[2] = $event => $data.selectedTeam = $event),
    id: "champion"
  }, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.teams, team => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("option", {
      key: team.id,
      value: team.name
    }, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(team.name), 9, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_10);
  }), 128))], 512), [[vue_runtime_esm_bundler["F" /* vModelSelect */], $data.selectedTeam]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_11, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_12, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_13, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.matches, match => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
      key: match.id
    }, [match.round === 1 && $options.isFutureMatch(match.time) ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_14, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_15, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_16, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event
    }, null, 8, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_17), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])]), Erovue_type_template_id_1d411fb8_scoped_true_hoisted_18, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_19, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event
    }, null, 8, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_20), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_21, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
  }), 128))])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_22, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_23, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_24, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.matches, match => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
      key: match.id
    }, [match.round === 2 && $options.isFutureMatch(match.time) ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_25, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_26, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_27, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event
    }, null, 8, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_28), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])]), Erovue_type_template_id_1d411fb8_scoped_true_hoisted_29, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_30, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event
    }, null, 8, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_31), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_32, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
  }), 128))])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_33, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_34, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_35, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.matches, match => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
      key: match.id
    }, [match.round === 3 && $options.isFutureMatch(match.time) ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_36, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_37, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_38, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event
    }, null, 8, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_39), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])]), Erovue_type_template_id_1d411fb8_scoped_true_hoisted_40, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_41, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event
    }, null, 8, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_42), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_43, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
  }), 128))])])]), Erovue_type_template_id_1d411fb8_scoped_true_hoisted_44], 32)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), $data.allUsersData && !$data.loading ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_45, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_46, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_47, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_48, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.allUsersData, data => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", {
      key: data.id
    }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(data.id), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(data.first_name), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(data.last_name), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
      onClick: $event => $options.showMatchData(data.match_data)
    }, "View", 8, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_49)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
      src: data.file_path,
      alt: "รูปภาพ",
      style: {
        "width": "100px",
        "height": "auto"
      }
    }, null, 8, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_50)])]);
  }), 128))])])])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), $data.showModal ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_51, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_52, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
    class: "close",
    onClick: _cache[4] || (_cache[4] = (...args) => $options.closeModal && $options.closeModal(...args))
  }, "×"), Erovue_type_template_id_1d411fb8_scoped_true_hoisted_53, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_54, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [Erovue_type_template_id_1d411fb8_scoped_true_hoisted_55, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.selectedMatchData, match => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", {
      key: match.id
    }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_56, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_57, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.predictionA), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_58, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_59, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.predictionB), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.time), 1)]);
  }), 128))])])])])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), $data.loading ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Erovue_type_template_id_1d411fb8_scoped_true_hoisted_60, Erovue_type_template_id_1d411fb8_scoped_true_hoisted_62)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)])]);
}
// CONCATENATED MODULE: ./src/components/Ero.vue?vue&type=template&id=1d411fb8&scoped=true

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Ero.vue?vue&type=script&lang=js

const matchesData = __webpack_require__("eeb9");
const TeamChameData = __webpack_require__("049d");
/* harmony default export */ var Erovue_type_script_lang_js = ({
  data() {
    return {
      firstName: '',
      lastName: '',
      file: null,
      formSubmitted: false,
      matches: matchesData,
      allUsersData: null,
      showModal: false,
      selectedMatchData: [],
      loading: false,
      selectedTeam: '',
      teams: TeamChameData,
      baseUrl: 'https://euro2024.egency.fun'
    };
  },
  computed: {
    selectedTeamId() {
      const selectedTeam = this.teams.find(team => team.name === this.selectedTeam);
      return selectedTeam ? selectedTeam.id : null;
    }
  },
  created() {
    this.loadFormData();
    const member = localStorage.getItem("keyMember");
    if (member) {
      window.location = '/leaderboard';
    }
  },
  methods: {
    isFutureMatch(matchTime) {
      const [dayMonth, time] = matchTime.split(' ');
      const [day, month] = dayMonth.split('/');
      const [hours, minutes] = time.split(':');
      const matchDate = new Date();
      matchDate.setDate(day);
      matchDate.setMonth(month - 1);
      matchDate.setHours(hours);
      matchDate.setMinutes(minutes);
      matchDate.setSeconds(0);
      return matchDate > new Date();
    },
    showMatchData(matchData) {
      this.selectedMatchData = JSON.parse(matchData);
      this.showModal = true;
    },
    closeModal() {
      this.showModal = false;
      this.selectedMatchData = [];
    },
    onFileChange(event) {
      const file = event.target.files[0];
      if (!file) {
        this.file = null;
        return;
      }
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = event => {
        const img = new Image();
        img.src = event.target.result;
        img.onload = () => {
          const canvas = document.createElement('canvas');
          const ctx = canvas.getContext('2d');
          const maxWidth = 800;
          const maxHeight = 800;
          let width = img.width;
          let height = img.height;
          if (width > height) {
            if (width > maxWidth) {
              height = Math.round(height *= maxWidth / width);
              width = maxWidth;
            }
          } else {
            if (height > maxHeight) {
              width = Math.round(width *= maxHeight / height);
              height = maxHeight;
            }
          }
          canvas.width = width;
          canvas.height = height;
          ctx.fillStyle = '#FFFFFF';
          ctx.fillRect(0, 0, width, height);
          ctx.drawImage(img, 0, 0, width, height);
          canvas.toBlob(blob => {
            const reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = () => {
              this.file = reader.result;
            };
          }, 'image/jpeg', 0.7);
        };
      };
    },
    async submitForm() {
      if (window.confirm("Confirm ?")) {
        var _this$file;
        if (this.firstName == "") {
          alert('Forgot your name ?');
          return;
        }
        this.loading = true;
        const formData = {
          first_name: this.firstName,
          last_name: this.lastName,
          matches: JSON.stringify(this.matches),
          file_data: (_this$file = this.file) !== null && _this$file !== void 0 ? _this$file : "",
          selected_team_id: this.selectedTeamId,
          selected_team_name: this.selectedTeam
        };
        console.log('formData:', formData);
        try {
          const response = await axios["a" /* default */].post(this.baseUrl + '/api/save_matches.php', formData, {
            headers: {
              'Content-Type': 'application/json'
            }
          });
          console.log('response:', response);
          if (response.data.status === 'success') {
            alert('ขอบคุณที่ร่วมสนุก ขอให้โชคดี รวยทรัพย์รับโชค เงินทองไหลมาเทมา');
            localStorage.setItem("keyMember", true);
            window.location = '/leaderboard';
          } else {
            alert(`บันทึกไม่สำเร็จ: ${response.data.message}`);
            console.error('Error:', response.data.message);
          }
        } catch (error) {
          if (error.response) {
            console.error('Error response:', error.response.data);
            console.error('Error status:', error.response.status);
            console.error('Error headers:', error.response.headers);
            alert(`บันทึกไม่สำเร็จ: ${error.response.data.message}`);
          } else if (error.request) {
            console.error('Error request:', error.request);
            alert('บันทึกไม่สำเร็จ: Network error. No response received.');
          } else {
            console.error('Error message:', error.message);
            alert(`บันทึกไม่สำเร็จ: ${error.message}`);
          }
        } finally {
          this.loading = false;
        }
      }
    },
    loadFormData() {
      const savedData = localStorage.getItem('formData');
      if (savedData) {
        try {
          this.allUsersData = JSON.parse(savedData);
          this.formSubmitted = true;
        } catch (error) {
          console.error('Error parsing JSON from localStorage:', error);
        }
      }
    },
    clearFormData() {
      console.log('clearFormData');
      localStorage.removeItem('formData');
      this.allUsersData = null;
      this.firstName = '';
      this.lastName = '';
      this.file = null;
      this.matches = matchesData;
      this.formSubmitted = false;
    },
    testForm() {
      this.firstName = 'Test';
      this.lastName = 'User';
      this.matches.forEach(match => {
        match.predictionA = Math.floor(Math.random() * 5);
        match.predictionB = Math.floor(Math.random() * 5);
      });
      const imgPath = __webpack_require__("b994");
      fetch(imgPath).then(res => res.blob()).then(blob => {
        this.file = new File([blob], 'Logo-Mazda.svg', {
          type: 'image/svg+xml'
        });
        this.submitForm();
      });
    }
  }
});
// CONCATENATED MODULE: ./src/components/Ero.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./src/components/Ero.vue?vue&type=style&index=0&id=1d411fb8&scoped=true&lang=css
var Erovue_type_style_index_0_id_1d411fb8_scoped_true_lang_css = __webpack_require__("dc00");

// CONCATENATED MODULE: ./src/components/Ero.vue







const Ero_exports_ = /*#__PURE__*/exportHelper_default()(Erovue_type_script_lang_js, [['render',Erovue_type_template_id_1d411fb8_scoped_true_render],['__scopeId',"data-v-1d411fb8"]])

/* harmony default export */ var Ero = (Ero_exports_);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader-v16/dist/templateLoader.js??ref--6!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/UserDataTable.vue?vue&type=template&id=445fa64e&scoped=true



const UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId = n => (Object(vue_runtime_esm_bundler["w" /* pushScopeId */])("data-v-445fa64e"), n = n(), Object(vue_runtime_esm_bundler["u" /* popScopeId */])(), n);
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_1 = {
  class: "container"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_2 = {
  class: "tab-buttons"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_3 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", null, "|", -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_4 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "40px",
    "font-weight": "bold"
  }
}, "EURO 2024"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "margin-top": "-10px",
    "font-weight": "bold"
  }
}, "TRUST YOUR GUTS, NOT THE FACTS")], -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_5 = {
  class: "tab-content"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_6 = {
  key: 0
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_7 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
  class: "registration-Rules",
  src: Txt_Rules_default.a,
  alt: "Registration Rules"
}, null, -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_8 = [UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_7];
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_9 = {
  key: 1
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_10 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "full-width-image-wrapper"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
  class: "full-width-image",
  src: leaderboard_default.a,
  alt: "Leaderboard"
})], -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_11 = {
  class: "container"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_12 = {
  key: 0,
  class: "loading-modal"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_13 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-content"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-spinner"
}, "loading...")], -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_14 = [UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_13];
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_15 = {
  key: 1,
  class: "table-container"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_16 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("thead", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tr", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", {
  style: {
    "font-size": "10px"
  }
}, "RANK"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", {
  style: {
    "font-size": "10px"
  }
}, "NAME"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", null, "PTS")])], -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_17 = ["onClick"];
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_18 = {
  key: 0,
  class: "modal"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_19 = {
  class: "modal-content"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_20 = {
  class: "modal-header"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_21 = {
  class: "user-info"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_22 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("br", null, null, -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_23 = {
  class: "score"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_24 = {
  class: "tabs"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_25 = {
  class: "tab-header"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_26 = {
  class: "tab-content"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_27 = {
  class: "table-header"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_28 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION", -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_29 = {
  style: {
    "font-size": "10px",
    "font-weight": "bold"
  }
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_30 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_31 = {
  class: "table2"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_32 = {
  key: 0,
  class: "match-row"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_33 = {
  class: "td_1"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_34 = {
  class: "td_2"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_35 = {
  class: "input-container"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_36 = ["onUpdate:modelValue"];
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_37 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_38 = {
  class: "td_4"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_39 = {
  class: "input-container"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_40 = ["onUpdate:modelValue"];
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_41 = {
  class: "td_5"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_42 = {
  class: "tab-content"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_43 = {
  class: "table-header"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_44 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION", -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_45 = {
  style: {
    "font-size": "10px",
    "font-weight": "bold"
  }
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_46 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_47 = {
  class: "table2"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_48 = {
  key: 0,
  class: "match-row"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_49 = {
  class: "td_1"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_50 = {
  class: "td_2"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_51 = {
  class: "input-container"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_52 = ["onUpdate:modelValue"];
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_53 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_54 = {
  class: "td_4"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_55 = {
  class: "input-container"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_56 = ["onUpdate:modelValue"];
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_57 = {
  class: "td_5"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_58 = {
  class: "tab-content"
};
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_59 = /*#__PURE__*/UserDataTablevue_type_template_id_445fa64e_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", null, "Content for Tab 3", -1));
const UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_60 = [UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_59];
function UserDataTablevue_type_template_id_445fa64e_scoped_true_render(_ctx, _cache, $props, $setup, $data, $options) {
  return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_1, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_2, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])({
      active: $data.currentTab === 'Rules'
    }),
    onClick: _cache[0] || (_cache[0] = $event => $data.currentTab = 'Rules')
  }, "Rules", 2), UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_3, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])({
      active: $data.currentTab === 'Leaderboard'
    }),
    onClick: _cache[1] || (_cache[1] = $event => $data.currentTab = 'Leaderboard')
  }, "Leaderboard", 2)]), UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_4]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_5, [$data.currentTab === 'Rules' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_6, UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_8)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), $data.currentTab === 'Leaderboard' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_9, [UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_10, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_11, [$data.loading ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_12, UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_14)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), !$data.loading && $data.allUsersData ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_15, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_16, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($options.rankedUsers, (user, index) => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", {
      key: user.id
    }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(user.rank), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
      onClick: $event => $options.showMatchData(user.match_data)
    }, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(user.first_name), 9, UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_17), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(user.TotalScore), 1)]);
  }), 128))])])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)]), $data.showModal ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_18, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_19, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_20, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_21, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", null, [Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.selectedUser.first_name), 1), UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_22, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("b", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.selectedUser.selected_team_name), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_23, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.selectedUser.TotalScore) + " PTS", 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
    class: "close",
    onClick: _cache[2] || (_cache[2] = (...args) => $options.closeModal && $options.closeModal(...args))
  }, "×")]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_24, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_25, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.selectedTab === 1
    }]),
    onClick: _cache[3] || (_cache[3] = $event => $data.selectedTab = 1)
  }, "QUALIFICATION", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(["tab-button", {
      active: $data.selectedTab === 2
    }]),
    onClick: _cache[4] || (_cache[4] = $event => $data.selectedTab = 2)
  }, "ROUND OF 16 FIXTURES", 2)]), Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_26, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])([1, 2, 3], round => {
    return Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
      class: "table-container",
      key: round
    }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_27, [UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_28, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_29, "MATCH " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(round) + " OF 3", 1)]), UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_30, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_31, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.selectedMatchData, match => {
      return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
        key: match.id
      }, [match.round === round ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_32, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_33, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_34, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_35, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        disabled: "",
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionA = $event
      }, null, 8, UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_36), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])])]), UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_37, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_38, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_39, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        disabled: "",
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionB = $event
      }, null, 8, UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_40), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_41, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
    }), 128))])])]);
  }), 64))], 512), [[vue_runtime_esm_bundler["H" /* vShow */], $data.selectedTab === 1]]), Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_42, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])([4], round => {
    return Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
      class: "table-container",
      key: round
    }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_43, [UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_44, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_45, "MATCH " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(round), 1)]), UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_46, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_47, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.selectedMatchData, match => {
      return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
        key: match.id
      }, [match.round === round ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_48, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_49, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_50, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_51, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        disabled: "",
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionA = $event
      }, null, 8, UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_52), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])])]), UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_53, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_54, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_55, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
        disabled: "",
        class: "small-input",
        type: "number",
        "onUpdate:modelValue": $event => match.predictionB = $event
      }, null, 8, UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_56), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_57, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
        class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
      }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
    }), 128))])])]);
  }), 64))], 512), [[vue_runtime_esm_bundler["H" /* vShow */], $data.selectedTab === 2]]), Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_58, UserDataTablevue_type_template_id_445fa64e_scoped_true_hoisted_60, 512), [[vue_runtime_esm_bundler["H" /* vShow */], $data.selectedTab === 3]])])])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)]);
}
// CONCATENATED MODULE: ./src/components/UserDataTable.vue?vue&type=template&id=445fa64e&scoped=true

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/UserDataTable.vue?vue&type=script&lang=js

/* harmony default export */ var UserDataTablevue_type_script_lang_js = ({
  data() {
    return {
      currentTab: 'Rules',
      resultMatches: [],
      allUsersData: [],
      selectedUser: null,
      showModal: false,
      selectedTab: 1,
      selectedMatchData: [],
      loading: false,
      TeamChame: {},
      useTest: false,
      // Flag to determine if test data should be used
      // baseUrl: 'https://euro2024.egency.fun/test',
      baseUrl: 'https://euro2024.egency.fun'
    };
  },
  async created() {
    await this.fetchResultMatches();
    await this.fetchResultCham();
    await this.fetchData();
    this.calculateScores();
  },
  computed: {
    rankedUsers() {
      // First, sort users by TotalScore (descending) and then by first_name (ascending)
      const sortedUsers = this.allUsersData.slice().sort((a, b) => {
        if (b.TotalScore === a.TotalScore) {
          return a.first_name.localeCompare(b.first_name);
        }
        return b.TotalScore - a.TotalScore;
      });

      // Assign ranks
      let rank = 1;
      let previousScore = null;
      let rankAdjustment = 0;
      return sortedUsers.map((user, index, array) => {
        if (previousScore !== null && user.TotalScore < previousScore) {
          rank += 1 + rankAdjustment;
          rankAdjustment = 0;
        } else if (previousScore !== null && user.TotalScore === previousScore) {
          rankAdjustment += 1;
        } else {
          rankAdjustment = 0;
        }
        previousScore = user.TotalScore;
        return {
          ...user,
          rank
        };
      });
    }
  },
  methods: {
    async fetchResultMatches() {
      try {
        const response = await axios["a" /* default */].get(this.baseUrl + '/json/resultMatchs.json');
        this.resultMatches = response.data;
      } catch (error) {
        console.error('Error fetching result matches data:', error);
      }
    },
    async fetchResultCham() {
      try {
        const response = await axios["a" /* default */].get(this.baseUrl + '/json/resultCham.json');
        this.TeamChame = response.data;
      } catch (error) {
        console.error('Error fetching result cham data:', error);
      }
    },
    async fetchData() {
      this.loading = true;
      try {
        const response = this.useTest ? {
          data: testData
        } : await axios["a" /* default */].get(this.baseUrl + '/api/get_users.php'); // Use test data if flag is set
        this.allUsersData = response.data.data;
      } catch (error) {
        console.error('Error fetching user data:', error);
        alert('Error fetching data');
      } finally {
        this.loading = false;
      }
    },
    calculateScores() {
      for (let user of this.allUsersData) {
        let score = 0;
        const data = JSON.parse(user.match_data);
        for (let Match of this.resultMatches) {
          const predictionMatch = data.find(m => m.id == Match.id && m.predictionA !== '' && m.predictionB !== '');
          if (!predictionMatch) continue;
          if (Match.predictionA === '' || Match.predictionB === '' || predictionMatch.predictionA === '' || predictionMatch.predictionB === '') continue;

          // win 3 score
          if (predictionMatch.predictionA == Match.predictionA && predictionMatch.predictionB == Match.predictionB) {
            predictionMatch.group !== 'Group 2' ? score += 3 : score += 6;
          }
          let winOrLostOrDrawPrediction = 3;
          let winOrLostOrDrawResult = 3;
          winOrLostOrDrawPrediction = predictionMatch.predictionA == predictionMatch.predictionB ? 2 : predictionMatch.predictionA > predictionMatch.predictionB ? 1 : predictionMatch.predictionA < predictionMatch.predictionB ? 0 : 3;
          winOrLostOrDrawResult = Match.predictionA == Match.predictionB ? 2 : Match.predictionA > Match.predictionB ? 1 : Match.predictionA < Match.predictionB ? 0 : 3;
          // win 1 score
          if (user.first_name == 'A+' && predictionMatch.group == 'Group 2') {
            debugger;
          }
          console.log(user.first_name);
          if (winOrLostOrDrawPrediction == winOrLostOrDrawResult && winOrLostOrDrawPrediction != 3 && winOrLostOrDrawResult != 3) {
            predictionMatch.group !== 'Group 2' ? score += 1 : score += 2;
          }
        }
        // win 12 score
        if (user.selected_team_name == this.TeamChame.name) {
          score += 12;
        }
        user.TotalScore = score;
      }
      this.allUsersData.sort((a, b) => b.TotalScore - a.TotalScore);
    },
    useTestData() {
      this.useTest = true;
      this.fetchData().then(() => this.calculateScores());
    },
    showMatchData(matchData) {
      this.selectedMatchData = JSON.parse(matchData);
      this.selectedUser = this.allUsersData.find(user => user.match_data === matchData);
      this.showModal = true;
    },
    closeModal() {
      this.showModal = false;
      this.selectedMatchData = [];
    }
  }
});
// CONCATENATED MODULE: ./src/components/UserDataTable.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./src/components/UserDataTable.vue?vue&type=style&index=0&id=445fa64e&scoped=true&lang=css
var UserDataTablevue_type_style_index_0_id_445fa64e_scoped_true_lang_css = __webpack_require__("ff39");

// CONCATENATED MODULE: ./src/components/UserDataTable.vue







const UserDataTable_exports_ = /*#__PURE__*/exportHelper_default()(UserDataTablevue_type_script_lang_js, [['render',UserDataTablevue_type_template_id_445fa64e_scoped_true_render],['__scopeId',"data-v-445fa64e"]])

/* harmony default export */ var UserDataTable = (UserDataTable_exports_);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader-v16/dist/templateLoader.js??ref--6!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Admin.vue?vue&type=template&id=26eb82ee&scoped=true



const Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId = n => (Object(vue_runtime_esm_bundler["w" /* pushScopeId */])("data-v-26eb82ee"), n = n(), Object(vue_runtime_esm_bundler["u" /* popScopeId */])(), n);
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_1 = {
  class: "container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_2 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "40px",
    "font-weight": "bold"
  }
}, "EURO 2024"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "margin-top": "-10px",
    "font-weight": "bold"
  }
}, "TRUST YOUR GUTS, NOT THE FACTS")], -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_3 = {
  class: "tab-buttons"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_4 = {
  class: "tab-content"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_5 = {
  key: 0
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_6 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
  class: "registration-Rules",
  src: Txt_Rules_default.a,
  alt: "Registration Rules"
}, null, -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_7 = [Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_6];
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_8 = {
  key: 1
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_9 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "full-width-image-wrapper"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("img", {
  class: "full-width-image",
  src: leaderboard_default.a,
  alt: "Leaderboard"
})], -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_10 = {
  class: "container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_11 = {
  key: 0,
  class: "loading-modal"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_12 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-content"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "loading-spinner"
}, "loading...")], -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_13 = [Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_12];
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_14 = {
  key: 1,
  class: "table-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_15 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("thead", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tr", null, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", {
  style: {
    "font-size": "10px"
  }
}, "RANK"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", {
  style: {
    "font-size": "10px"
  }
}, "NAME"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("th", null, "PTS")])], -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_16 = ["onClick"];
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_17 = {
  key: 0,
  class: "modal"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_18 = {
  class: "modal-content"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_19 = {
  class: "modal-header"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_20 = {
  class: "user-info"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_21 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("br", null, null, -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_22 = {
  class: "score"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_23 = {
  class: "table-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_24 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "table-header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "10px",
    "font-weight": "bold"
  }
}, "MATCH 1 OF 3")], -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_25 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_26 = {
  class: "table2"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_27 = {
  key: 0,
  class: "match-row"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_28 = {
  class: "td_1"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_29 = {
  class: "td_2"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_30 = {
  class: "input-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_31 = ["onUpdate:modelValue"];
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_32 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_33 = {
  class: "td_4"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_34 = {
  class: "input-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_35 = ["onUpdate:modelValue"];
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_36 = {
  class: "td_5"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_37 = {
  class: "table-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_38 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "table-header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "10px",
    "font-weight": "bold"
  }
}, "MATCH 2 OF 3")], -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_39 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_40 = {
  class: "table2"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_41 = {
  key: 0,
  class: "match-row"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_42 = {
  class: "td_1"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_43 = {
  class: "td_2"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_44 = {
  class: "input-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_45 = ["onUpdate:modelValue"];
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_46 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_47 = {
  class: "td_4"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_48 = {
  class: "input-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_49 = ["onUpdate:modelValue"];
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_50 = {
  class: "td_5"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_51 = {
  class: "table-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_52 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", {
  class: "table-header"
}, [/*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "20px",
    "font-weight": "bold"
  }
}, "SCORE PREDICTION"), /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", {
  style: {
    "font-size": "10px",
    "font-weight": "bold"
  }
}, "MATCH 3 OF 3")], -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_53 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("hr", {
  style: {
    "margin-top": "-32px"
  },
  noshade: ""
}, null, -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_54 = {
  class: "table2"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_55 = {
  key: 0,
  class: "match-row"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_56 = {
  class: "td_1"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_57 = {
  class: "td_2"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_58 = {
  class: "input-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_59 = ["onUpdate:modelValue"];
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_60 = /*#__PURE__*/Adminvue_type_template_id_26eb82ee_scoped_true_withScopeId(() => /*#__PURE__*/Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
  class: "td_3"
}, ":", -1));
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_61 = {
  class: "td_4"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_62 = {
  class: "input-container"
};
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_63 = ["onUpdate:modelValue"];
const Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_64 = {
  class: "td_5"
};
function Adminvue_type_template_id_26eb82ee_scoped_true_render(_ctx, _cache, $props, $setup, $data, $options) {
  return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", null, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_1, [Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_2, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_3, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])({
      active: $data.currentTab === 'Rules'
    }),
    onClick: _cache[0] || (_cache[0] = $event => $data.currentTab = 'Rules')
  }, "Rules", 2), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("button", {
    class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])({
      active: $data.currentTab === 'Leaderboard'
    }),
    onClick: _cache[1] || (_cache[1] = $event => $data.currentTab = 'Leaderboard')
  }, "Leaderboard", 2)])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_4, [$data.currentTab === 'Rules' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_5, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_7)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), $data.currentTab === 'Leaderboard' ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_8, [Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_9, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_10, [$data.loading ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_11, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_13)) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true), !$data.loading && $data.allUsersData ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_14, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", null, [Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_15, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($options.rankedUsers, (user, index) => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", {
      key: user.id
    }, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(user.rank), 1), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", {
      onClick: $event => $options.showMatchData(user.match_data)
    }, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(user.first_name), 9, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_16), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(user.TotalScore), 1)]);
  }), 128))])])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)]), $data.showModal ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_17, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_18, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_19, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_20, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", null, [Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.selectedUser.first_name), 1), Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_21, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("b", null, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.selectedUser.selected_team_name), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("p", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_22, Object(vue_runtime_esm_bundler["D" /* toDisplayString */])($data.selectedUser.TotalScore) + " PTS", 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
    class: "close",
    onClick: _cache[2] || (_cache[2] = (...args) => $options.closeModal && $options.closeModal(...args))
  }, "×")]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_23, [Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_24, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_25, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_26, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.selectedMatchData, match => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
      key: match.id
    }, [match.round === 2 ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_27, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_28, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_29, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_30, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      disabled: "",
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event
    }, null, 8, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_31), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])])]), Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_32, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_33, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_34, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      disabled: "",
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event
    }, null, 8, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_35), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_36, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
  }), 128))])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_37, [Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_38, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_39, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_40, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.selectedMatchData, match => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
      key: match.id
    }, [match.round === 2 ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_41, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_42, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_43, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_44, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      disabled: "",
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event
    }, null, 8, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_45), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])])]), Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_46, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_47, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_48, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      disabled: "",
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event
    }, null, 8, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_49), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_50, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
  }), 128))])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_51, [Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_52, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_53, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("table", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_54, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("tbody", null, [(Object(vue_runtime_esm_bundler["t" /* openBlock */])(true), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], null, Object(vue_runtime_esm_bundler["z" /* renderList */])($data.selectedMatchData, match => {
    return Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])(vue_runtime_esm_bundler["a" /* Fragment */], {
      key: match.id
    }, [match.round === 3 ? (Object(vue_runtime_esm_bundler["t" /* openBlock */])(), Object(vue_runtime_esm_bundler["f" /* createElementBlock */])("tr", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_55, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_56, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagA}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamA), 1)]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_57, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_58, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      disabled: "",
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionA = $event
    }, null, 8, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_59), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionA]])])]), Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_60, Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_61, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("div", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_62, [Object(vue_runtime_esm_bundler["J" /* withDirectives */])(Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("input", {
      disabled: "",
      class: "small-input",
      type: "number",
      "onUpdate:modelValue": $event => match.predictionB = $event
    }, null, 8, Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_63), [[vue_runtime_esm_bundler["G" /* vModelText */], match.predictionB]])])]), Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("td", Adminvue_type_template_id_26eb82ee_scoped_true_hoisted_64, [Object(vue_runtime_esm_bundler["g" /* createElementVNode */])("span", {
      class: Object(vue_runtime_esm_bundler["p" /* normalizeClass */])(`flag-icon flag-icon-${match.flagB}`)
    }, null, 2), Object(vue_runtime_esm_bundler["i" /* createTextVNode */])(" " + Object(vue_runtime_esm_bundler["D" /* toDisplayString */])(match.teamB), 1)])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)], 64);
  }), 128))])])])])])) : Object(vue_runtime_esm_bundler["e" /* createCommentVNode */])("", true)]);
}
// CONCATENATED MODULE: ./src/components/Admin.vue?vue&type=template&id=26eb82ee&scoped=true

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader-v16/dist??ref--1-1!./src/components/Admin.vue?vue&type=script&lang=js

/* harmony default export */ var Adminvue_type_script_lang_js = ({
  data() {
    return {
      currentTab: 'Rules',
      resultMatches: [],
      allUsersData: [],
      selectedUser: null,
      showModal: false,
      // baseUrl: 'https://euro2024.egency.fun/test',
      baseUrl: 'https://euro2024.egency.fun',
      selectedMatchData: [],
      loading: false,
      TeamChame: {},
      useTest: false // Flag to determine if test data should be used
    };
  },
  async created() {
    await this.fetchResultMatches();
    await this.fetchResultCham();
    await this.fetchData();
    this.calculateScores();
  },
  computed: {
    rankedUsers() {
      // First, sort users by TotalScore (descending) and then by first_name (ascending)
      const sortedUsers = this.allUsersData.slice().sort((a, b) => {
        if (b.TotalScore === a.TotalScore) {
          return a.first_name.localeCompare(b.first_name);
        }
        return b.TotalScore - a.TotalScore;
      });

      // Assign ranks
      let rank = 1;
      let previousScore = null;
      let rankAdjustment = 0;
      return sortedUsers.map((user, index, array) => {
        if (previousScore !== null && user.TotalScore < previousScore) {
          rank += 1 + rankAdjustment;
          rankAdjustment = 0;
        } else if (previousScore !== null && user.TotalScore === previousScore) {
          rankAdjustment += 1;
        } else {
          rankAdjustment = 0;
        }
        previousScore = user.TotalScore;
        return {
          ...user,
          rank
        };
      });
    }
  },
  methods: {
    async fetchResultMatches() {
      try {
        const response = await axios["a" /* default */].get(this.baseUrl + '/json/resultMatchs.json');
        this.resultMatches = response.data;
      } catch (error) {
        console.error('Error fetching result matches data:', error);
      }
    },
    async fetchResultCham() {
      try {
        const response = await axios["a" /* default */].get(this.baseUrl + '/json/resultCham.json');
        this.TeamChame = response.data;
      } catch (error) {
        console.error('Error fetching result cham data:', error);
      }
    },
    async fetchData() {
      this.loading = true;
      try {
        const response = this.useTest ? {
          data: testData
        } : await axios["a" /* default */].get(this.baseUrl + '/api/get_users.php'); // Use test data if flag is set
        this.allUsersData = response.data.data;
      } catch (error) {
        console.error('Error fetching user data:', error);
        alert('Error fetching data');
      } finally {
        this.loading = false;
      }
    },
    calculateScores() {
      for (let user of this.allUsersData) {
        let score = 0;
        const data = JSON.parse(user.match_data);
        for (let Match of this.resultMatches) {
          const predictionMatch = data.find(m => m.id == Match.id && m.predictionA !== '' && m.predictionB !== '');
          if (!predictionMatch) continue;
          if (Match.predictionA === '' || Match.predictionB === '' || predictionMatch.predictionA === '' || predictionMatch.predictionB === '') continue;

          // win 3 score
          if (predictionMatch.predictionA == Match.predictionA && predictionMatch.predictionB == Match.predictionB) {
            score += 3;
          }
          let winOrLostOrDrawPrediction = 3;
          let winOrLostOrDrawResult = 3;
          winOrLostOrDrawPrediction = predictionMatch.predictionA == predictionMatch.predictionB ? 2 : predictionMatch.predictionA > predictionMatch.predictionB ? 1 : predictionMatch.predictionA < predictionMatch.predictionB ? 0 : 3;
          winOrLostOrDrawResult = Match.predictionA == Match.predictionB ? 2 : Match.predictionA > Match.predictionB ? 1 : Match.predictionA < Match.predictionB ? 0 : 3;
          // win 1 score
          if (winOrLostOrDrawPrediction == winOrLostOrDrawResult && winOrLostOrDrawPrediction != 3 && winOrLostOrDrawResult != 3) {
            score += 1;
          }
        }
        // win 12 score
        if (user.selected_team_name == this.TeamChame.name) {
          score += 12;
        }
        user.TotalScore = score;
      }
      this.allUsersData.sort((a, b) => b.TotalScore - a.TotalScore);
    },
    useTestData() {
      this.useTest = true;
      this.fetchData().then(() => this.calculateScores());
    },
    showMatchData(matchData) {
      this.selectedMatchData = JSON.parse(matchData);
      this.selectedUser = this.allUsersData.find(user => user.match_data === matchData);
      this.showModal = true;
    },
    closeModal() {
      this.showModal = false;
      this.selectedMatchData = [];
    }
  }
});
// CONCATENATED MODULE: ./src/components/Admin.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./src/components/Admin.vue?vue&type=style&index=0&id=26eb82ee&scoped=true&lang=css
var Adminvue_type_style_index_0_id_26eb82ee_scoped_true_lang_css = __webpack_require__("b7f0");

// CONCATENATED MODULE: ./src/components/Admin.vue







const Admin_exports_ = /*#__PURE__*/exportHelper_default()(Adminvue_type_script_lang_js, [['render',Adminvue_type_template_id_26eb82ee_scoped_true_render],['__scopeId',"data-v-26eb82ee"]])

/* harmony default export */ var Admin = (Admin_exports_);
// CONCATENATED MODULE: ./src/router/index.js








const routes = [{
  path: "/",
  // path: '/test/',
  name: "App",
  component: App
},
// {
//   path: '/leaderboard',
//   name: 'UserDataTable',
//   component: UserDataTable,
// },
{
  path: "/leaderboard",
  name: "Leaderboard",
  component: Leaderboard
}, {
  path: "/rules",
  name: "Rules",
  component: Rules
}, {
  path: "/jojoadminvip",
  // path: '/test/jojoadminvip',
  name: "jojoadminVIP",
  component: Admin
}, {
  path: "/login",
  // path: '/test/login',
  name: "Login",
  component: Login
}];
const router = Object(vue_router["a" /* createRouter */])({
  history: Object(vue_router["b" /* createWebHistory */])(""),
  routes
});
/* harmony default export */ var src_router = (router);
// EXTERNAL MODULE: ./node_modules/vuex/dist/vuex.esm-browser.js
var vuex_esm_browser = __webpack_require__("5502");

// CONCATENATED MODULE: ./src/store/index.js

const store = Object(vuex_esm_browser["a" /* createStore */])({
  state: {
    message: 'Hello Vue!'
  },
  mutations: {},
  actions: {},
  modules: {}
});
/* harmony default export */ var src_store = (store);
// CONCATENATED MODULE: ./src/main.js




Object(vue_runtime_esm_bundler["c" /* createApp */])(App).use(src_router).use(src_store).mount('#app');

/***/ }),

/***/ "66e1":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/Txt-Register.ae3f3cc2.png";

/***/ }),

/***/ "6f11":
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":39,\"group\":\"Group 5\",\"teamA\":\"Czech5\",\"flagA\":\"cz\",\"teamB\":\"Turkey5\",\"flagB\":\"tr\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"27/6 02:00\",\"round\":7}]");

/***/ }),

/***/ "73dc":
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":37,\"group\":\"Group 3\",\"teamA\":\"Czech3\",\"flagA\":\"cz\",\"teamB\":\"Turkey3\",\"flagB\":\"tr\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"27/6 02:00\",\"round\":5}]");

/***/ }),

/***/ "7ed9":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "8105":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/vml.940cdd34.jpeg";

/***/ }),

/***/ "9d49":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/Logo-Euro.c3c94527.png";

/***/ }),

/***/ "b7f0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Admin_vue_vue_type_style_index_0_id_26eb82ee_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("fedd");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Admin_vue_vue_type_style_index_0_id_26eb82ee_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Admin_vue_vue_type_style_index_0_id_26eb82ee_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "b994":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/Logo-Mazda.d10c52ec.svg";

/***/ }),

/***/ "dc00":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Ero_vue_vue_type_style_index_0_id_1d411fb8_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("eda2");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Ero_vue_vue_type_style_index_0_id_1d411fb8_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Ero_vue_vue_type_style_index_0_id_1d411fb8_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "e52a":
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":38,\"group\":\"Group 4\",\"teamA\":\"Czech4\",\"flagA\":\"cz\",\"teamB\":\"Turkey4\",\"flagB\":\"tr\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"27/6 02:00\",\"round\":6}]");

/***/ }),

/***/ "ea0f":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Leaderboard_vue_vue_type_style_index_0_id_2ae074ba_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("fca5");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Leaderboard_vue_vue_type_style_index_0_id_2ae074ba_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_Leaderboard_vue_vue_type_style_index_0_id_2ae074ba_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "eda2":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "eeb9":
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":10,\"group\":\"Group 1\",\"teamA\":\"Romania\",\"flagA\":\"ro\",\"teamB\":\"Ukraine\",\"flagB\":\"ua\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"17/6 20:00\",\"round\":1},{\"id\":5,\"group\":\"Group 1\",\"teamA\":\"Belgium\",\"flagA\":\"be\",\"teamB\":\"Slovakia\",\"flagB\":\"sk\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"17/6 23:00\",\"round\":1},{\"id\":11,\"group\":\"Group 1\",\"teamA\":\"Austria\",\"flagA\":\"at\",\"teamB\":\"France\",\"flagB\":\"fr\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"18/6 02:00\",\"round\":1},{\"id\":6,\"group\":\"Group 1\",\"teamA\":\"Turkey\",\"flagA\":\"tr\",\"teamB\":\"Georgia\",\"flagB\":\"ge\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"18/6 23:00\",\"round\":1},{\"id\":12,\"group\":\"Group 1\",\"teamA\":\"Portugal\",\"flagA\":\"pt\",\"teamB\":\"Czech\",\"flagB\":\"cz\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"19/6 02:00\",\"round\":1},{\"id\":13,\"group\":\"Group 1\",\"teamA\":\"Croatia\",\"flagA\":\"hr\",\"teamB\":\"Albania\",\"flagB\":\"al\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"19/6 20:00\",\"round\":2},{\"id\":14,\"group\":\"Group 1\",\"teamA\":\"Germany\",\"flagA\":\"de\",\"teamB\":\"Hungary\",\"flagB\":\"hu\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"19/6 23:00\",\"round\":2},{\"id\":15,\"group\":\"Group 1\",\"teamA\":\"Scotland\",\"flagA\":\"gb-sct\",\"teamB\":\"Switzerland\",\"flagB\":\"ch\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"20/6 02:00\",\"round\":2},{\"id\":16,\"group\":\"Group 1\",\"teamA\":\"Slovenia\",\"flagA\":\"si\",\"teamB\":\"Serbia\",\"flagB\":\"rs\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"20/6 20:00\",\"round\":2},{\"id\":17,\"group\":\"Group 1\",\"teamA\":\"Denmark\",\"flagA\":\"dk\",\"teamB\":\"England\",\"flagB\":\"gb-eng\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"20/6 23:00\",\"round\":2},{\"id\":18,\"group\":\"Group 1\",\"teamA\":\"Spain\",\"flagA\":\"es\",\"teamB\":\"Italy\",\"flagB\":\"it\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"21/6 02:00\",\"round\":2},{\"id\":19,\"group\":\"Group 1\",\"teamA\":\"Slovakia\",\"flagA\":\"sk\",\"teamB\":\"Ukraine\",\"flagB\":\"ua\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"21/6 20:00\",\"round\":2},{\"id\":20,\"group\":\"Group 1\",\"teamA\":\"Poland\",\"flagA\":\"pl\",\"teamB\":\"Austria\",\"flagB\":\"at\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"21/6 23:00\",\"round\":2},{\"id\":21,\"group\":\"Group 1\",\"teamA\":\"Netherlands\",\"flagA\":\"nl\",\"teamB\":\"France\",\"flagB\":\"fr\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"22/6 02:00\",\"round\":2},{\"id\":22,\"group\":\"Group 1\",\"teamA\":\"Georgia\",\"flagA\":\"ge\",\"teamB\":\"Czech\",\"flagB\":\"cz\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"22/6 20:00\",\"round\":2},{\"id\":23,\"group\":\"Group 1\",\"teamA\":\"Turkey\",\"flagA\":\"tr\",\"teamB\":\"Portugal\",\"flagB\":\"pt\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"22/6 23:00\",\"round\":2},{\"id\":24,\"group\":\"Group 1\",\"teamA\":\"Belgium\",\"flagA\":\"be\",\"teamB\":\"Romania\",\"flagB\":\"ro\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"23/6 02:00\",\"round\":2},{\"id\":25,\"group\":\"Group 1\",\"teamA\":\"Switzerland\",\"flagA\":\"ch\",\"teamB\":\"Germany\",\"flagB\":\"de\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"24/6 02:00\",\"round\":3},{\"id\":26,\"group\":\"Group 1\",\"teamA\":\"Scotland\",\"flagA\":\"gb-sct\",\"teamB\":\"Hungary\",\"flagB\":\"hu\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"24/6 02:00\",\"round\":3},{\"id\":27,\"group\":\"Group 1\",\"teamA\":\"Albania\",\"flagA\":\"al\",\"teamB\":\"Spain\",\"flagB\":\"es\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"25/6 02:00\",\"round\":3},{\"id\":28,\"group\":\"Group 1\",\"teamA\":\"Croatia\",\"flagA\":\"hr\",\"teamB\":\"Italy\",\"flagB\":\"it\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"25/6 02:00\",\"round\":3},{\"id\":29,\"group\":\"Group 1\",\"teamA\":\"France\",\"flagA\":\"fr\",\"teamB\":\"Poland\",\"flagB\":\"pl\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"25/6 23:00\",\"round\":3},{\"id\":30,\"group\":\"Group 1\",\"teamA\":\"Netherlands\",\"flagA\":\"nl\",\"teamB\":\"Austria\",\"flagB\":\"at\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"25/6 23:00\",\"round\":3},{\"id\":31,\"group\":\"Group 1\",\"teamA\":\"Denmark\",\"flagA\":\"dk\",\"teamB\":\"Serbia\",\"flagB\":\"rs\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"26/6 02:00\",\"round\":3},{\"id\":32,\"group\":\"Group 1\",\"teamA\":\"England\",\"flagA\":\"gb-eng\",\"teamB\":\"Slovenia\",\"flagB\":\"si\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"26/6 02:00\",\"round\":3},{\"id\":323,\"group\":\"Group 1\",\"teamA\":\"Slovakia\",\"flagA\":\"sk\",\"teamB\":\"Romania\",\"flagB\":\"ro\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"26/6 23:00\",\"round\":3},{\"id\":33,\"group\":\"Group 1\",\"teamA\":\"Ukraine\",\"flagA\":\"ua\",\"teamB\":\"Belgium\",\"flagB\":\"be\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"26/6 23:00\",\"round\":3},{\"id\":34,\"group\":\"Group 1\",\"teamA\":\"Georgia\",\"flagA\":\"ge\",\"teamB\":\"Portugal\",\"flagB\":\"pt\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"27/6 02:00\",\"round\":3},{\"id\":35,\"group\":\"Group 1\",\"teamA\":\"Czech\",\"flagA\":\"cz\",\"teamB\":\"Turkey\",\"flagB\":\"tr\",\"predictionA\":\"\",\"predictionB\":\"\",\"time\":\"27/6 02:00\",\"round\":3}]");

/***/ }),

/***/ "f61c":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/Txt-Rules.33ac9e4c.png";

/***/ }),

/***/ "fca5":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "fe23":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/profile-placeholder.316cdb9f.png";

/***/ }),

/***/ "fedd":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "ff39":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_UserDataTable_vue_vue_type_style_index_0_id_445fa64e_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("53f1");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_UserDataTable_vue_vue_type_style_index_0_id_445fa64e_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_v16_dist_index_js_ref_1_1_UserDataTable_vue_vue_type_style_index_0_id_445fa64e_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ })

/******/ });
//# sourceMappingURL=app.4ffff904.js.map