<?php
header('Content-Type: application/json');

// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: " . $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);

    exit(0);
}

// Enable error reporting for debugging (disable this in production)
ini_set('display_errors', 1);
error_reporting(E_ALL);

// Database connection
$host = '151.106.114.3';
$db   = 'u996656745_euro2024';
$user = 'u996656745_euro2024';
$pass = 'rH567$tjfp';
$charset = 'utf8mb4';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    error_log("Database connection failed: " . $e->getMessage());
    responseWithError("Database connection error.");
    exit;
}

function responseWithError($message) {
    echo json_encode(['error' => true, 'message' => $message]);
    exit;
}

function createTablesIfNotExists($pdo) {
    $createUsersTable = "
    CREATE TABLE IF NOT EXISTS users (
        id INT AUTO_INCREMENT PRIMARY KEY,
        first_name VARCHAR(255) NOT NULL,
        last_name VARCHAR(255) NOT NULL,
        file_path BLOB NULL,
        selected_team_id INT NULL,
        selected_team_name VARCHAR(255) NULL,
        created_at DATETIME DEFAULT CURRENT_TIMESTAMP
    )";
    
    $createMatchesTable = "
    CREATE TABLE IF NOT EXISTS matches (
        id INT AUTO_INCREMENT PRIMARY KEY,
        user_id INT,
        match_data TEXT,
        created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY (user_id) REFERENCES users(id)
    )";
    
    $pdo->exec($createUsersTable);
    $pdo->exec($createMatchesTable);
}

function saveData($data, $pdo) {
    try {
        // Log the received data
        error_log('Received data: ' . print_r($data, true));

        $firstName = $data['first_name'] ?? null;
        $lastName = $data['last_name'] ?? null;
        $matches = $data['matches'] ?? null;
        $filePath = $data['file_data'] ?? null;
        $selectedTeamId = $data['selected_team_id'] ?? null;
        $selectedTeamName = $data['selected_team_name'] ?? null;
        $createdAt = date('Y-m-d H:i:s'); // Current datetime

        // Log the individual values
        error_log("First Name: $firstName");
        error_log("Last Name: $lastName");
        error_log("Matches: $matches");
        error_log("File Path: $filePath");
        error_log("Selected Team ID: $selectedTeamId");
        error_log("Selected Team Name: $selectedTeamName");

        // Check required fields and return specific error messages
        $missingFields = [];
        if (!$firstName) $missingFields[] = 'first_name';
        if (!$lastName) $missingFields[] = 'last_name';
        if (!$matches) $missingFields[] = 'matches';

        if (!empty($missingFields)) {
            return ['status' => 'error', 'message' => 'Required fields are missing: ' . implode(', ', $missingFields)];
        }

        // Check for existing user with same name and surname
        $stmt = $pdo->prepare("SELECT * FROM users WHERE first_name = :first_name AND last_name = :last_name");
        $stmt->execute(['first_name' => $firstName, 'last_name' => $lastName]);
        $existingUser = $stmt->fetch();

        if ($existingUser) {
            // User exists, update matches data
            $userId = $existingUser['id'];

            // Fetch current matches data
            $stmt = $pdo->prepare("SELECT match_data FROM matches WHERE user_id = :user_id");
            $stmt->execute(['user_id' => $userId]);
            $existingMatches = $stmt->fetchColumn();

            // Decode existing matches data
            $existingMatchesData = json_decode($existingMatches, true) ?: [];
            $newMatchesData = json_decode($matches, true) ?: [];

            // Merge new matches data with existing data
            $updatedMatchesData = $existingMatchesData;

            foreach ($newMatchesData as $newMatch) {
                $found = false;
                foreach ($updatedMatchesData as &$existingMatch) {
                    if ($existingMatch['id'] === $newMatch['id']) {
                        $existingMatch = $newMatch;
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $updatedMatchesData[] = $newMatch;
                }
            }

            $updatedMatchesJson = json_encode($updatedMatchesData);

            // Update matches data
            $stmt = $pdo->prepare("UPDATE matches SET match_data = :match_data WHERE user_id = :user_id");
            $stmt->execute(['match_data' => $updatedMatchesJson, 'user_id' => $userId]);
        } else {
            // Insert new user
            $stmt = $pdo->prepare("INSERT INTO users (first_name, last_name, file_path, selected_team_id, selected_team_name, created_at) VALUES (:first_name, :last_name, :file_path, :selected_team_id, :selected_team_name, :created_at)");
            $stmt->execute(['first_name' => $firstName, 'last_name' => $lastName, 'file_path' => $filePath, 'selected_team_id' => $selectedTeamId, 'selected_team_name' => $selectedTeamName, 'created_at' => $createdAt]);
            $userId = $pdo->lastInsertId();

            // Insert matches
            $stmt = $pdo->prepare("INSERT INTO matches (user_id, match_data, created_at) VALUES (:user_id, :match_data, :created_at)");
            $stmt->execute(['user_id' => $userId, 'match_data' => $matches, 'created_at' => $createdAt]);
        }

        return ['status' => 'success'];
    } catch (Exception $e) {
        error_log("Error in saveData: " . $e->getMessage());
        return ['status' => 'error', 'message' => "An error occurred while saving the data: " . $e->getMessage()];
    }
}

createTablesIfNotExists($pdo);

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        // Get form data
        $postData = json_decode(file_get_contents('php://input'), true);

        // Log postData for debugging
        error_log(print_r($postData, true));

        // Save data
        $response = saveData($postData, $pdo);
        
        // Return the response as JSON
        echo json_encode($response);
    } catch (Exception $e) {
        error_log("Error processing request: " . $e->getMessage());
        responseWithError("An error occurred processing the request.");
    }
} else {
    responseWithError("Invalid request method.");
}
