<?php
header('Content-Type: application/json');

// Database connection
$host = '151.106.114.3';
$db   = 'u996656745_euro2024';
$user = 'u996656745_euro2024';
$pass = 'rH567$tjfp';
$charset = 'utf8mb4';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    error_log("Database connection failed: " . $e->getMessage());
    responseWithError("Database connection error.");
    exit;
}

function responseWithError($message) {
    echo json_encode(['error' => true, 'message' => $message]);
    exit;
}

// Check if the request method is GET
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    try {
        // Fetch data from the database
        $stmt = $pdo->query("
            SELECT users.id, users.first_name, users.last_name, users.file_path, users.selected_team_id, users.selected_team_name, matches.match_data 
            FROM users 
            JOIN matches ON users.id = matches.user_id
        ");
        $users = $stmt->fetchAll();

        // Return the users data as JSON
        echo json_encode(['data' => $users]);
    } catch (Exception $e) {
        error_log("Error fetching data: " . $e->getMessage());
        responseWithError("An error occurred while fetching data: " . $e->getMessage());
    }
} else {
    responseWithError("Invalid request method.");
}
?>
